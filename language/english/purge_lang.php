<?php
$lang = array(
    // Modules page
    'purge_module_name' => "Purge",
    'purge_module_description' => "Enables AJAX functions required for the Purge extension's settings page.",

    // Settings form
    'site' => "Site",
    'channel' => "Channel",
    'submit' => "Submit",

    '' => ''
);
?>
