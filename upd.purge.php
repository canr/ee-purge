<?php

include_once(realpath( dirname(__FILE__)."/PurgeLogger.php" ));
 
/**
 * Class to install, uninstall, and update this module.
 */
class Purge_upd
{
    # ###########
    # Member Data
    # ###########
    // Related Objects
    private $EE;
    private $logger;

    // Module Info
    public $module_name = "Purge";
    public $version = "2.2.0";


    # #########
    # Built-ins
    # #########
    /**
     * Constructor.
     *
     * Hooks ExpressionEngine.
     */
    public function __construct()
    {
        // Make a local reference to the ExpressionEngine super object
        $this->EE =& get_instance();

        // Instantiate logger
        $this->logger = new PurgeLogger();
    }


    # ################
    # Update Functions
    # ################
    /**
     * Install the module.
     *
     * Inserts the module's name, version, and whether it has a backend/tab in exp_modules.
     *
     * @return Returns TRUE on successful installation.
     */
    public function install ()
    {
    	// Add module info to modules table
    	$data = array(
    		'module_name' => $this->module_name,
    		'module_version' => $this->version,
    		'has_cp_backend' => 'n',
    		'has_publish_fields' => 'n'
    	);
    	$this->EE->db->insert('modules', $data);

    	// Register module actions in the actions table
    	$actions = array(
    		array('class' => 'Purge', 'method' => 'ProcessPurgeRequest' ),
            array('class' => 'Purge', 'method' => 'GetChannels'),
            array('class' => 'Purge', 'method' => 'GetSettings'),
            array('class' => 'Purge', 'method' => 'SetSettings')
    	);
    	$this->EE->db->insert_batch('actions', $actions);

        return TRUE;
    }
    	
    /**
     * Uninstall the module.
     *
     * Deletes the module's record and its registered actions.
     *
     * @return TRUE on success.
     */
    public function uninstall()
    {
	    // Get the module's ID
	    $this->EE->db->select('module_id');
	    $query = $this->EE->db->get_where('modules', array('module_name' => $this->module_name));
	
	    // Delete member groups
	    $this->EE->db->where('module_id', $query->row('module_id'));
	    $this->EE->db->delete('module_member_groups');
	
	    // Delete module from the table of registered modules
	    $this->EE->db->where('module_name', $this->module_name);
	    $this->EE->db->delete('modules');
	
	    // Delete the module's registered actions
	    $this->EE->db->where('class', $this->module_name);
	    $this->EE->db->delete('actions');
	    
        return TRUE;
    }

    /**
     * Update the module.
     *
     * This doesn't actually do anything at the moment.
     *
     * @todo Implement proper update functionality.
     * @param string $current String containing the current module version as a float.
     * @returns boolean Whether or not the module needs to be updated.
     */
    public function update($current = '')
    {
	    return FALSE;
    }
}

/* End of file upd.purge.php */
/* Location: /system/expressionengine/third_party/purge/upd.purge.php */
