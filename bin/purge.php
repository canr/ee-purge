<?php
/**
 * Command line utility to purge URLs from the cache.
 */

require realpath(dirname(__FILE__).'/../helpers/varnish_helper.php');

// Parse options
$options = getopt('p:', array('port:'));

// Port
if (isset($options['port']))
	$port = (int)$options['port'];
else if (isset($options['p']))
	$port = (int)$options['p'];
else    
	$port = 80;

// URL
$url = $argv[$argc-1];

// Send request
echo "Purging {$url}:{$port}";
send_purge_request($url, $port);

// End of file varnish_helper.php
// Location: ./system/expressionengine/third_party/purge/helpers/varnish_helper.php
