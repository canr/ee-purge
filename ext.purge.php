<?php
include_once(realpath( dirname(__FILE__)."/PurgeLogger.php" ));
require_once(realpath( dirname(__FILE__)."/PurgeSettings.php" ));

/**
 * Class to manage the Purge add-on's hook functions.
 */
class Purge_ext
{	
    # ###########
    # Member Data
    # ###########
    // Related objects
	public $EE; //!< Reference to the main EE object.
    private $logger; //!< Reference to a logger object.
    public $settings; //!< Reference to a PurgeSettings object.

    // Extension info
	public $description		= 'Sends purge header to the caching server after entry submission and deletion.'; //!< Description of the extension's functionality.
	public $docs_url		= ''; //!< URL for the extension's documentation.
	public $name			= 'Purge'; //!< Name of the extension.
	public $settings_exist	= 'y'; //!< Whether or not the extension has a settings page in the Control Panel.
	public $version			= '2.2.0'; //!< Version number of the extension.

    /**
     * Hooks table
     *
     * @see http://ellislab.com/expressionengine/user-guide/development/extension_hooks/index.html
     */
	public $hooks = array(
      'entry_submission_end'	=> 'purge_submitted_entry',
      'delete_entries_loop'		=> 'purge_deleted_entry'
    );


    # #########
    # Built-Ins
    # #########
	/**
	 * Constructor
	 */
	public function __construct()
	{
        // Hook EE
		$this->EE =& get_instance();

        // Instantiate the logger
        $this->logger = new PurgeLogger();
        $this->logger->Log("======== Constructed Purge_ext ========");

        // Instantiate the settings object
        $this->settings = new PurgeSettings();
	}
	

    # ######################
    # Extension Requirements
    # ######################
	/**
	 * Activate Extension
	 *
	 * Enters the extension's info into the exp_extensions table.
	 *
	 */
	public function activate_extension()
	{
        $this->logger->Log("Activating extension.");

        // Create settings table
        $this->EE->load->dbforge();
        $this->EE->dbforge->add_field(array(
            'channel_id' => array('type' => 'int', 'unsigned' => TRUE),
            'purge' => array('type' => 'boolean'),
            'uris' => array('type' => 'text')
        ));
        $this->EE->dbforge->add_key('channel_id', TRUE);
        $this->EE->dbforge->create_table('purge_settings');
        $this->logger->Log("Created the purge_settings table.");

        // Insert hooks into extensions table
		foreach ($this->hooks as $hook => $method)
		{
            // Set up extension/ hook data
			$data = array(
				'class'		=> __CLASS__,
				'method'	=> $method,
				'hook'		=> $hook,
				'settings'	=> '',
				'version'	=> $this->version,
				'enabled'	=> 'y'
			);
			
            // Insert data in extensions table
			$this->EE->db->insert('extensions', $data);
            $this->logger->Log("Added method $method() to hook $hook.");
		}

	}	

	/**
	 * Removes extension information from the exp_extensions table.
	 */
	public function disable_extension()
	{
        // Drop settings table
        $this->EE->load->dbforge();
        $this->EE->dbforge->drop_table('purge_settings');
        $this->logger->Log("Dropped the purge_settings table.");

        // Remove from exp_extensions
		$this->EE->db->where('class', __CLASS__);
		$this->EE->db->delete('extensions');
        $this->logger->Log("Disabled extension.");
	}

	/**
	 * Update Extension
	 *
	 * This function performs any necessary db updates when the extension page is visited.
	 *
	 * @return mixed Void on update / FALSE if no update required.
	 */
	public function update_extension($current = '')
	{
		if ($current == '' OR $current == $this->version)
		{
			return FALSE;
		}
	}	
	

    # ########
    # Settings
    # ########
    /**
     * Loads the settings form.
     */
    public function settings_form ()
    {
        $vars = array();

        // Get ACTion ID URLs for the Purge functions
 		$vars['GetChannelsURL'] = 'index.php?ACT='.$this->EE->cp->fetch_action_id('Purge', 'GetChannels');
 		$vars['GetSettingsURL'] = 'index.php?ACT='.$this->EE->cp->fetch_action_id('Purge', 'GetSettings');
 		$vars['SetSettingsURL'] = 'index.php?ACT='.$this->EE->cp->fetch_action_id('Purge', 'SetSettings');
        $this->logger->Log("Set ACTion URLs.");

        // Send in the site listing
        $vars['sites'] = $this->settings->GetSites();
        $this->logger->Log("Set site listing.");

        // Load view file
        return $this->EE->load->view('extension_settings', $vars, TRUE);
    }


    # #####
    # Purge
    # #####
    /**
     * Sends a purge request to Varnish when an entry is submitted.
     *
     * @param int $entry_id ID of the entry to be purged.
     * @param array $meta Array of metadata associated with the entry. Indecies include 'channel_id', 'author_id', 'site_id', 'ip_address', 'title', 'url_title', 'entry_date', 'edit_date', 'versioning_enabled', 'year', 'month', 'day', 'expiration_date', 'comment_expiration_date', 'recent_comment_date', 'sticky', 'status', and 'allow comments'.
     * @param array $data Array of entry data. Indecies include 'entry_id', 'channel_id', 'autosave_entry_id', 'member_group', 'layout_preview', 'new_channel', 'cp_call', 'revision_post', 'ping_servers', 'ping', 'options', 'author', and all field_id_* and field_ft_* associated with the channel.
     */
    public function purge_submitted_entry ($entry_id, $meta, $data)
    {
        $this->logger->log("Purging submitted entry $entry_id.");
        $this->PurgeEntry($entry_id, $data['channel_id']);
    }

    /**
     * Sends a purge request to Varnish when an entry is deleted.
     *
     * @param  int $entry_id  ID of the entry to be purged.
     * @param  int $channel_id ID of the channel to be purged.
     */
    public function purge_deleted_entry ($entry_id, $channel_id)
    {
        $this->logger->log("Purging deleted entry $entry_id.");
        $this->PurgeEntry($entry_id, $channel_id);
    }

    /**
     * Sends a purge request to Varnish.
     *
     * @param  int $entry_id  ID of the entry to be purged.
     * @param  int $channel_id ID of the channel to be purged.
     */
	public function PurgeEntry ($entry_id, $channel_id)
	{
        // Should this entry be purged?
        if (!$this->ShouldBePurged($channel_id))
        {
            $this->logger->Log("Not purging entry $entry_id, channel $channel_id.");
            return FALSE;
        }
        $this->logger->Log("Purging entry $entry_id, channel $channel_id.");

        
        // Send purge requests
        $purgeUris = $this->GetPurgeUris($entry_id, $channel_id);
        foreach ($purgeUris as $purgeUri)
        {
            $this->SendPurgeRequest($purgeUri);
        }
	}

    /**
     * Checks whether or not the given channel should be purged.
     * 
     * @param int $channel_id ID of the channel being checked.
     * @return  bool Whether or not the given channel should be purged.
     */
    public function ShouldBePurged ($channel_id)
    {
        $channel_settings = $this->settings->GetSettings($channel_id);
        $shouldBePurged = $channel_settings['purge'];
        return $shouldBePurged;
    }

    /**
     * Gets the URIs to be purged for the entry.
     * 
     * @param [type] $entry_id [description]
     * @param [type] $channel_id [description]
     * @return array 
     */
    public function GetPurgeUris ($entry_id, $channel_id)
    {
        // Get raw URIs
        $channel_settings = $this->settings->GetSettings($channel_id);
        $rawUris = $channel_settings['uris'];

        // Replace 
        try
        {
            // Get site_id for replace_tags
            $site_id = $this->FetchSiteIdByChannelId($channel_id);

            // Set URIs
            $purgeUris = $this->replace_tags($rawUris, $entry_id, $site_id, $channel_id);

            // Log success
            $this->logger->Log("Generated purge URIs.");
            $this->logger->Log($purgeUris);
        }
        catch (Exception $e)
        {
            // Set lack of URIs
            $purgeUris = array();

            // Log failure
            $this->logger->Log($e->getMessage());
            $this->logger->EELog("Unable to get purge URIs for entry $entry_id.\n".$e->getMessage());
        }

        return $purgeUris;
    }

    /**
     * Replaces EE-style tags in the URIs array with entry data.
     *
     * This function currently defines the following tags:
     *
     *     {base_url} - URL base of the site to which the entry which initiated the purge belongs.
     *     {entry_id} - ID of the entry which initiated the purge.
     *     {site_id} - ID of the site to which the entry which initiated the purge belongs.
     *     {channel_id} - ID of the channel to which the entry which initiated the purge belongs.
     *     {structure_uri} - URI of the entry which initiated the purge.
     *
     * @param array $uris The array of URIs to modify.
     * @param int $entry_id The ID of the entry being purged.
     * @param int $site_id The ID of the site to which the entry being purged belongs.
     * @param int $channel_id The ID of the channel to which the entry being purged belongs.
     */
    private function replace_tags ($uris, $entry_id, $site_id, $channel_id)
    {
        // Declare tags
        $tags = array(
            'entry_id' => '/{entry_id}/',
            'site_id' => '/{site_id}/',
            'channel_id' => '/{channel_id}/',
            'base_url' => '/{base_url}/',
            'url_title' => '/{url_title}/',
            'structure_uri' => '/{structure_uri}/'
        );

        // Fetch values
        try
        {
            $base_url = $this->FetchBaseUrl($site_id);
        }
        catch (Exception $e)
        {
            $base_url = '{base_url}';
        }

        try
        {
            $structure_uri = $this->FetchStructureUri($entry_id);
        }
        catch (Exception $e)
        {
            $structure_uri = '{structure_uri}';
        }

        try
        {
            $url_title = $this->FetchUrlTitle($entry_id);
        }
        catch (Exception $e)
        {
            $url_title = '{url_title}';
        }

        // Declare values
        $values = array(
            'site_id' => $site_id,
            'channel_id' => $channel_id,
            'entry_id' => $entry_id,
            'base_url' => $base_url,
            'url_title' => $url_title,
            'structure_uri' => $structure_uri
        );

        // Replace tags with data
        return preg_replace($tags, $values, $uris);
    }

    /**
     * Sets up, sends, and handles a request to purge a given URI.
     *
     * @param string $purgeUri The URI to be purged from the cache.
     */
    public function SendPurgeRequest ($purgeUri)
    {
        $this->logger->Log("Purging URI $purgeUri.");

        // Load Varnish helper
        if (!function_exists('send_purge_request'))
            $this->EE->load->helper('varnish');

        try
        {
            // Send request
            $HttpStatusCode = send_purge_request($purgeUri);

            // Log status
            if ($HttpStatusCode == 200)
            {
                $this->logger->Log("Successfully purged $purgeUri.");
            }
            elseif ($HttpStatusCode == 404)
            {
                $this->logger->Log("Page $purgeUri not in cache.");
            }
            else
            {
                $msg = "Failed to purge $purgeUri. HTTP Status: $HttpStatusCode.";
                $this->logger->Log($msg);
                $this->logger->EELog($msg);
            }

            return $HttpStatusCode;
        }
        catch (Exception $e)
        {
            // Log failure
            $this->logger->Log($e->getMessage());
            $this->logger->EELog("Unable to purge entry $entry_id at URI $purgeURI.\n".$e->getMessage(), TRUE);

            return FALSE;
        }
    }



    # ##############
    # Data Retrieval
    # ##############
    /**
     * Fetches the ID of the site to which a given channel belongs.
     * 
     * @param int $channel_id Channel whose site ID is being fetched.
     * @return int ID of the site to which the given channel belongs.
     */
    private function FetchSiteIdByChannelId ($channel_id)
    {
        // Query
        $result = $this->EE->db
            -> from ('exp_channels')
            -> where ('channel_id', $channel_id)
            -> select (
                'site_id'
            )
            -> get() -> result_array();
        
        // No results?
        if (empty($result))
            throw new Exception("No site found for channel $channel_id.");

        return $result[0]['site_id'];
    }

    /**
     * Fetches a site's base URL.
     *
     * @param int $site_id ID of the site whose base URL is to be retrieved.
     * @return string base_url string. Includes protocol and excludes trailing slash. E.x. "http://msue.dev.anr.msu.edu".
     */
    private function FetchBaseUrl ($site_id)
    {
        // Query
        $results = $this->EE->db
            -> select (
                'site_description'
            )
            -> from ('exp_sites')
            -> where ('site_id', $site_id)
            -> get() -> result_array();
        
        // No results?
        if ( empty($results) )
            throw new Exception ("No site found with site_id $site_id.");

        // Strip trailing slash
        $base_url = $results[0]['site_description'];
        if ( substr($base_url, -1) == '/' )
            $base_url = substr($base_url, 0, -1);

        return $base_url;
    }

    /**
     * Fetches an entry's URL title.
     * 
     * @param int $entry_id The ID of the entry whose URL title is to be fetched.
     * @return string URL title of the given entry.
     */
    private function FetchUrlTitle ($entry_id)
    {
        // Query
        $result = $this->EE->db
            -> from ('exp_channel_titles')
            -> where ('entry_id', $entry_id)
            -> select (
                'url_title'
            )
            -> get() -> result_array();
        
        // No results?
        if (empty($result))
            throw new Exception("No entry with ID $entry_id found.");

        return $result[0]['url_title'];
    }
 
    /**
     * Fetches an entry's Structure URI.
     *
     * @param int $entry_id The ID of the entry whose Structure URI is to be fetched.
     * @return string Structure URI of the given entry.
     */
    private function FetchStructureUri ($entry_id)
    {
        // Query
        $results = $this->EE->db
            -> select (
                'uri'
            )
            -> from ('exp_structure_listings')
            -> where ('entry_id', $entry_id)
            -> get() -> result_array();
        
        // No results?
        if ( empty($results) )
            throw new Exception ("No structure listing found for entry_id $entry_id.");

        return $results[0]['uri'];
    }

}

/* End of file ext.purge.php */
/* Location: /system/expressionengine/third_party/purge/ext.purge.php */
