<style type="text/css">
<?php require_once(realpath(dirname(__FILE__)."/../css/accessory_purge_varnish.css")); ?>
</style>

<div id="about-message">
	<p>This website uses caching to improve performance and fault tolerance. Because of this, content changes may take up to 24 hours to display. As an editor, you can 'see through' the cache by editing in one browser tab while viewing the website in another. You can also 'drop' pages from the cache by URL using this tool if your content is time-sensitive:</p>
</div>

<br/>

<?=form_open('#', array( 'id' => 'purge-form') )?>

    <div id="purge-fields">
        <div class="purge-field">
            <input type="text" name="urls[]" class="purge-url" size="100">
            <button type="button" class="purge-add-remove">+</button>
            <div class="purge-status"></div>
        </div>
    </div>

    <br/><?=form_button(array(
        'name' => 'purge-submit',
        'id' => 'purge-submit',
        'content' => 'Purge URLs'
    ))?>
    <?=form_button(array(
        'name' => 'purge-clear',
        'id' => 'purge-clear',
        'content' => 'Clear'
    ))?>

<?=form_close()?>

<script type="text/javascript">
    (function (PurgeVarnish, $, undefined) {
        PurgeVarnish.Urls = {
            ProcessRequest: "<?=$ProcessRequestUrl?>"
        };
    }( window.PurgeVarnish = window.PurgeVarnish || {}, jQuery));   
</script>

<script type="text/javascript" charset="utf-8">
<?php require_once(realpath(dirname(__FILE__)."/../js/accessory_purge_varnish.js")); ?>
</script>
