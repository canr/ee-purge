<style type="text/css">
<?php require_once(realpath(dirname(__FILE__)."/../css/extension_settings.css")); ?>
</style>

<form id="purge-settings">
    <fieldset id="channel-select">

        <div class="field">
            <label for="site">Site</label>
            <select id="site" class="control" name="site"><option value="none"></option>
            <?php foreach ($sites as $ID => $name) { ?>
                <option value="<?=$ID?>"><?=$name?></option>
            <?php } ?>
            </select>
        </div>

        <div id="channel-field" class="field">
            <label for="channel">Channel</label>
            <select id="channel" class="control" name="channel">
                <option value="none"></option>
            </select>
        </div>

    </fieldset>


    <fieldset id="settings">

        <div class="field">
            <label for="purge-enabled">Purge</label>
            <span class="control">
                <label for="purge-enabled-yes">Yes</label><input type="radio" value="yes" name="purge-enabled" id="purge-enabled-yes">
                <label for="purge-enabled-no">No</label><input type="radio" value="no" name="purge-enabled" id="purge-enabled-no">
            </span>
        </div>

        <div class="field">
            <label for="uri-patterns">URI Patterns</label>
            <textarea name="uri-patterns" id="uri-patterns" class="control" rows="5"></textarea>
        </div>

    </fieldset>

    <button id="save" class="save" name="save" type="button">Save</button>
</form>


<script>
<?php require_once(realpath(dirname(__FILE__)."/../js/extension_settings.js")); ?>
</script>
