(function( Purge, $, undefined ) {
    var currSite,
        currChannel;

    Purge.showChannels = function (site_id) {
        if (site_id == "none")
            return false;

        // Clear channel select list and settings fields
        clearChannels();
        clearSettings();

        // Populate channel select list
        $.ajax({
            url: '<?=$GetChannelsURL?>',
            type: 'POST',
            data: {
                site_id: site_id
            },
            dataType: "json",

            success: function (data, textStatus, jqXHR) {
                // Error handling
                if (typeof(data.error) != 'undefined') {
                    console.error(data.error);
                    return;
                }

                // List channels
                for (var i = 0; i < data.length; i++) {
                    var channel = data[i];
                    $('#channel').append('<option value="'+channel.id+'">'+channel.name+'</option>');
                }

                // Show channels
                $('#channel-field').show(500);

                // Set current site
                currSite = site_id;
            }
        });
    };

    Purge.showSettings = function (channel_id) {
        if (channel_id == "none")
            return false;

        // Clear settings form
        clearSettings();

        // Get settings object
        $.ajax({
            url: '<?=$GetSettingsURL?>',
            type: 'POST',
            data: {
                channel_id: channel_id 
            },
            dataType: "json",

            success: function (data, textStatus, jqXHR) {
                // Error handling
                if (typeof(data.error) != 'undefined') {
                    console.error(data.error);
                    return;
                }

                // Set Purge radio
                if (data.purge)
                    $('#purge-enabled-yes').prop('checked', true);
                else
                    $('#purge-enabled-no').prop('checked', true);

                // Place URIs in URI Patterns textarea
                if (data.uris.length != 0)
                    $('#uri-patterns').val( data.uris.join("\n") );

                // Show settings
                $('#settings, #save').show(500);

                // Set current channel
                currChannel = channel_id;
            }
        });
    }

    function clearChannels () {
        // Hide channel field
        $('#channel-field').hide();

        // Clear
        $('#channel').empty();

        // Write empty option
        $('#channel').append('<option value="none"></option>');
    }

    function clearSettings () {
        // Hide settings area
        $('#settings, #save').hide();
         
        // Clear Purge radio
        $('#purge-enabled-yes').prop('checked', false);
        $('#purge-enabled-no').prop('checked', false);

        // Clear URI Patterns textarea
        $('#uri-patterns').val('');

        // Clear save button
        Purge.clearSaved();
    }

    Purge.save = function () {
        if (! currSite || !currChannel)
            return false;

        // Get form data
        var purge = $('#purge-enabled-yes').prop('checked');
        var uris = $('#uri-patterns').val().split("\n");

        // Send form data by AJAX POST
        $.ajax({
            url: '<?=$SetSettingsURL?>',
            type: 'POST',
            data: {
                channel_id: currChannel,
                purge: purge,
                uris: uris
            },
            dataType: "json",

            success: function (data, textStatus, jqXHR) {
                // Error handling
                if (typeof(data) != 'object' && typeof(data.error) != 'undefined') {
                    console.error(data.error);
                    return;
                }

                // Set the saved state
                setSaved();
            }
        });
    }

    Purge.clearSaved = function () {
        // Button text
        $('#save').html('Save');

        // Enable
        $('#save').prop('disabled', false);
    }

    function setSaved () {
        // Button text
        $('#save').html('Saved');

        // Disable
        $('#save').prop('disabled', true);
    }


    // Event handlers
    $('#site').on('change', function () { Purge.showChannels($(this).val()); } );
    $('#channel').on('change', function () { Purge.showSettings($(this).val()); } );
    $('#uri-patterns').on('input', Purge.clearSaved );
    $('#save').on('click', Purge.save );

    // Hide elements
    $('#channel-field, #settings, #save').hide();


}( window.Purge = window.Purge || {}, jQuery ));