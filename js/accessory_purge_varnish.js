(function (PurgeVarnish, $, undefined) {

    PurgeVarnish.onAdd = function () {
        // Change this button's text from + to -
        $(this).text('-');

        // Change this button's event handler from add to remove
        $(this).off( 'click.PurgeVarnish' );
        $(this).on( 'click.PurgeVarnish', PurgeVarnish.onRemove );

        // Add a purge-field to the purge-fields container
        $('#purge-fields').append(createField());

    }

    function createField () {
        // Instantiate a new jQuery-wrapped field
        var field$ = $('<div class="purge-field"> <input type="text" name="urls[]" class="purge-url" size="100"> <button type="button" class="purge-add-remove">+</button> <div class="purge-status"></div> </div>');

        // Set the add event handler
        field$.find('.purge-add-remove').on( 'click.PurgeVarnish', PurgeVarnish.onAdd );

        return field$;
    }

    PurgeVarnish.onRemove = function () {
        // Remove this button's parent purge-field from the DOM
        $(this).parent().remove();
    }

    PurgeVarnish.onSubmit = function () {
        var fields = $('.purge-field');
        $('.purge-field').each( function (i) {
            var url = $(this).find('.purge-url').val();
            var stat = $(this).find('.purge-status');
            

            // Send URL by POST, receive XMLHTTPRequest object
            stat.text("Sending...");
            $.ajax(PurgeVarnish.Urls.ProcessRequest, {
                type: 'POST',
                data: { url: url },
                cache: false,
                context: stat,

                success: function (response) {
                    $(this).text(response);
                },

                error: function (jqXHR, textStatus, errorThrown) {
                    $(this).text('Error: '+errorThrown+'.');
                }

            });
        });
    }

    PurgeVarnish.onClear = function () {
        // Delete all fields
        $('#purge-fields').empty();

        // Create a new field
        $('#purge-fields').append(createField());
    }

    // Event handlers
    $("#purge-submit").click( PurgeVarnish.onSubmit );
    $("#purge-clear").click( PurgeVarnish.onClear );
    $(".purge-add-remove").on( 'click.PurgeVarnish', PurgeVarnish.onAdd );

}( window.PurgeVarnish = window.PurgeVarnish || {}, jQuery));