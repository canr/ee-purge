<?php
include_once(realpath( dirname(__FILE__)."/PurgeLogger.php" ));
require_once(realpath( dirname(__FILE__)."/PurgeSettings.php" ));

/**
 * @file mod.purge.php
 * @ingroup Purge Module
 */

/**
 * Class to manage Purge's AJAX response functions.
 */
class Purge
{
    # ###########
    # Member Data
    # ###########
    // Releated Objects
	private $EE; //!< Reference to the ExpressionEngine system object.
    private $settings; //!< Purge Settings object.

	# ###########
	# Constructor
	# ###########
	/**
	 * Constructor
	 */
	public function __construct()
	{
		// Hook ExpressionEngine
		$this->EE =& get_instance();

        // Instantiate settings object
        $this->settings = new PurgeSettings();

        // Instatiate logger
        $this->logger = new PurgeLogger();
        $this->logger->Log("======== Constructed Purge ========");
	}


    # #######################
    # AJAX Response Functions
    # #######################
    /**
     * Sends a purge request to Varnish for the given URL.
     */
    public function ProcessPurgeRequest ()
    {
        $this->logger->Log("ProcessPurgeRequest");
        // Load helpers
        $this->EE->load->helper('varnish');
        $this->logger->Log("Loaded the Varnish helper.");

        // Get URL strings
        $rawUrl = $this->EE->input->post('url', TRUE);
        $this->logger->Log("Processing request for raw URL $rawUrl.");
        if (substr($rawUrl, -1) == "/") # Each request will actually send $rawURL and $rawURL."/", so we don't want any trailing slashes here.
            $rawUrl = substr($rawUrl, 0, -1);
        if (!strpos($rawUrl, '://')) # If we don't have a scheme, parse_url will fail.
            $rawUrl = 'http://'.$rawUrl;

        $url = parse_url( $rawUrl );
        $this->logger->Log("Parsed URL:");
        $this->logger->Log($url);
        if (!$url || !isset($url['host']) ) # Malformed URL
        {
            $this->logger->Log("Malformed URL: $rawUrl.");
            echo 'Malformed URL.';
            return;
        }
        
        // Get requested site_id
        try
        {
            $baseUrl = $url['scheme']."://".$url['host']."/";
            $site_id = $this->FetchSiteIdByBaseUrl( $baseUrl );
            $this->logger->Log("Purging from site $site_id.");
        }
        catch (Exception $e)
        {
            $this->logger->Log("Failed to fetch site for URL $baseUrl.");
            $this->logger->Log($e->getMessage());
            if (strpos($e->getMessage(), 'not found') !== FALSE)
            {
                echo 'Site not in cache.';
            }
            elseif (strpos($e->getMessage(), 'Multiple conflicting sites') !== FALSE)
            {
                echo 'Multiple conflicting sites found for URL.';
            }
            return;
        }

        // Permissions check
        if ($this->UserMayPurge($site_id))
        {
            $this->logger->Log("User has permission to purge from site $site_id.");
            // Send request
            try
            {
                // URL port defaults to 80
                $port = isset($url['port']) ?
                    $url['port'] :
                    80;

                // Send the purge headers through the helper
                $this->logger->Log("Sending purge request to $rawUrl on port $port.");
                $status1 = send_purge_request($rawUrl, $port);
                $status2 = send_purge_request($rawUrl."/", $port);
                
                // Check success
                if ($status1 == 200 || $status2 == 200)
                {
                    $this->logger->Log("Purged $rawUrl.");
                    echo 'Success!';
                }
                elseif ($status1 == 404 || $status2 == 404)
                {
                    // Page not in cache
                    $this->logger->Log("URL $rawUrl not in cache.");
                    echo 'Page not in cache.';
                }
                else
                {
                    $this->logger->Log("Failed to purge $rawUrl.");
                    $this->logger->Log("Status: $status2");
                    echo 'Failed to purge URL.';
                }
                
                // Send headers
                return;
            }
            catch (Exception $e)
            {
                // Page not in cache
                $this->logger->Log("URL $rawUrl not in cache.");
                echo 'Page not in cache.';
                return;
            }
        }
        else
        {
            // Deny
            $this->logger->Log("User does not have permission to purge from site $site_id.");
            echo 'Permission denied.';
            return;
        }
    }

    /**
     * Gets a list of the given site's channels.
     *
     * Outputs the given site's channels as JSON object in the form:
     * [
     *     {
     *         id: $channel_id,
     *         name: $channel_name
     *     }
     * ]
     */
    public function GetChannels ()
    {
        // Get site ID
        if (! $site_id = $this->EE->input->post('site_id', TRUE) )
        {
            $this->EE->output->send_ajax_response(array('error' => 'No site ID given.'));
            return;
        }

        // Query channels
        try
        {
            $channels = $this->settings->GetChannels($site_id);
        }
        catch (Exception $e)
        {
            $this->EE->output->send_ajax_response(array('error' => 'No channels for given site.'));
            return;
        }

        // Format for nice JSON conversion
        foreach ($channels as $id => $name)
        {
            $response[] = array(
                'id' => $id,
                'name' => $name
            );
        }

        // Convert to JSON and send
        $this->EE->output->send_ajax_response($response);
    }

    /**
     * Gets a given channel's preferences.
     *
     * Outputs the given channel's preferences as JSON object in the form:
     * {
     *     purge: $purge,
     *     uris: [
     *         $uri
     *     ]
     * }
     */
    public function GetSettings ()
    {
        // Get channel ID
        if (! $channel_id = $this->EE->input->post('channel_id', TRUE) )
        {
            $this->EE->output->send_ajax_response(array('error' => 'No channel ID given.'));
            return;
        }

        // Query Settings
        try
        {
            $response = $this->settings->GetSettings($channel_id);
        }
        catch (Exception $e)
        {
            $response = array(
                'purge' => FALSE,
                'uris' => array()
            );
        }

        // Convert to JSON and send
        $this->EE->output->send_ajax_response($response);
    }

    /**
     * Sets a given channel's preferences.
     */
    public function SetSettings ()
    {
        $this->logger->Log("Setting preferences.");
        $this->logger->Log($_POST);

        // Check whether parameters are set
        foreach (array('channel_id', 'purge', 'uris') as $param)
        {
            if (! isset( $_POST[ $param ] ) )
            {
                $msg = "Parameter '$param' not set.";
                $this->EE->output->send_ajax_response(array('error' => $msg));
                $this->logger->Log($msg);
                return;
            }
        }

        // Get channel ID
        $channel_id = (int)$this->EE->input->post('channel_id', TRUE);
        $this->logger->Log("channel_id: $channel_id");

        // Get purge
        $purge = ( strtolower( $this->EE->input->post('purge', TRUE) ) === "true" );
        $purgeStr = $purge ? "TRUE" : "FALSE";
        $this->logger->Log("purge: $purgeStr");

        // Get URIs
        $uris = $this->EE->input->post('uris', TRUE);
        $this->logger->Log("uris: ");
        $this->logger->Log($uris);

        // Write to DB
        $this->settings->SetSettings($channel_id, $purge, $uris);
    }


    # ##############
    # Data Retrieval
    # ##############
    /**
     * Gets the given site's site_id.
     * 
     * @param string $baseUrl The base URL of the site to get the ID of, in the form "http://example.com/".
     * @return int Returns the site's ID.
     */
    private function FetchSiteIdByBaseUrl ($baseUrl)
    {
        if ($baseUrl == 'http://varnish.dev.anr.msu.edu/')
            $baseUrl = 'http://canr.dev.anr.msu.edu/';
        $this->logger->Log("Fetching site_id for base URL $baseUrl.");

        // Query
        $IDResult = $this->EE->db
            -> from ('exp_sites')
            -> where ('site_description', $baseUrl)
            -> select ('site_id')
            -> get() -> result_array();
        
        // Error checking
        if (count($IDResult) == 0)
            throw new Exception("Site with base URL $baseUrl not found.");
        if (count($IDResult) > 1)
            throw new Exception("Multiple conflicting sites found for base URL $baseUrl.");
        
        // Return ID
        return $IDResult[0]['site_id'];
    }
    
    /**
     * Determines whether the current user has permissions to purge pages from the given site.
     * 
     * @param int $site_id The site_id of the site to check.
     * @return boolean Whether or not the current user has permissions for the site.
     */
    private function UserMayPurge ($site_id)
    {
        // Get group_id
        $group_id = $this->EE->session->userdata('group_id');
        $this->logger->Log("Determining whether user from group $group_id may purge on site $site_id.");
        
        // Query
        $permResult = $this->EE->db
            -> from ('exp_member_groups')
            -> where ('site_id', $site_id)
                -> where ('group_id', $group_id)
            -> select ('can_access_cp')
            -> get() -> result_array();

        $this->logger->Log($permResult);
        
        // Error check
        if (empty($permResult))
            return FALSE;
        
        // Return permission
        return ($permResult[0]['can_access_cp'] == "y");
    }
    
}

/* End of file mod.purge.php */
/* Location: /system/expressionengine/third_party/purge/mod.purge.php */
