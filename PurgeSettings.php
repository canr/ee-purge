<?php
include_once(realpath( dirname(__FILE__)."/PurgeLogger.php" ));

/**
 * Class to store and manage the Purge extension's settings.
 */
class PurgeSettings
{
    # #########
    # Built-Ins
    # #########
    /**
     * Constructor.
     */
    public function __construct ()
    {
        // Hook EE
        $this->EE =& get_instance();

        // Instantiate logger
        $this->logger = new PurgeLogger();
        $this->logger->Log("======== Constructed PurgeSettings ========");
    }


    # ##############
    # Data Retrieval
    # ##############
    /**
     * Queries the database for all sites.
     *
     * @return array 2D array of DB records, each with site_id and site_name.
     */
    private function QuerySites ()
    {
        // Query
        $results = $this->EE->db
            -> select (
                'site_id, '.
                'site_label AS site_name'
            )
            -> from ('exp_sites as sites')
            -> order_by ('site_label')
            -> get() -> result_array();

        // No results?
        if ( empty($results) )
            throw new Exception ("No sites found.");

        // Compress into 1D array
        foreach ($results as $result)
        {
            $sites[ $result['site_id'] ] = $result['site_name'];
        }

        return $sites;
    }

    /**
     * Queries the database for a given site's channels.
     *
     * @param int $site_id ID of the site whose channels are to be returned.
     * @return array Array of channels in the form:
     * array( $id => $name )
     */
    private function QueryChannels ($site_id)
    {
        // Query
        $results = $this->EE->db
            -> select (
                'channel_id, '.
                'channel_title AS channel_name'
            )
            -> from ('exp_channels as channels')
            -> where ('site_id', $site_id)
            -> order_by ('channel_name')
            -> get() -> result_array();

        // No results?
        if ( empty($results) )
            throw new Exception ("No channels found.");

        // Compress into 1D array
        foreach ($results as $result)
        {
            $channels[ $result['channel_id'] ] = $result['channel_name'];
        }

        return $channels;
    }


    # ########
    # Data I/O
    # ########
    /**
     * Loads a channel's settings from the DB.
     *
     * @param int $channel_id ID of the channel whose settings are to be loaded.
     * @return array Array of the channel's settings in the form:
     * array(
     *     'purge' => $purge,
     *     'uris' => array(
     *         $uri
     *     )
     * )
     */
    public function Load ($channel_id)
    {
        // Query
        $results = $this->EE->db
            -> from ('exp_purge_settings')
            -> where ('channel_id', $channel_id)
            -> select (
                'purge, '.
                'uris'
            )
            -> get() -> result_array();
        
        // No results?
        if ( empty($results) )
            throw new Exception ("No settings for channel $channel_id.");

        $this->logger->Log("Pulled settings from the database.");
        $this->logger->Log($results);

        // Format
        $settings = array(
            'purge' => (boolean)$results[0]['purge'],
            'uris' => unserialize($results[0]['uris'])
        );
        $this->logger->Log("Formatted settings.");
        $this->logger->Log($settings);

        return $settings;
    }

    /**
     * Writes a channel's settings to the database.
     *
     * @param int $channel_id ID of the channel whose settings
     * @param boolean $purge Whether or not the channel should purge automatically.
     * @param array $uris Array of URI strings to be purged when the channel is purged.
     */
    public function Write ($channel_id, $purge, $uris)
    {
        // Create record array
        $settings = array(
            'channel_id' => (int)$channel_id,
            'purge' => (boolean)$purge,
            'uris' => serialize($uris)
        );
        $this->logger->Log("Writing settings for channel $channel_id to database.");
        $this->logger->Log($settings);

        // Does the channel already have a settings record?
        $record_exists = (boolean)$this->EE->db
            -> from ('exp_purge_settings')
            -> where('channel_id', $channel_id)
            -> count_all_results();

        if ($record_exists)
        {
            // Update
            $this->logger->Log("Updating record for channel $channel_id.");
            $success = $this->EE->db
                -> where('channel_id', $channel_id)
                -> update ('exp_purge_settings', $settings);
        }
        else
        {
            // Insert
            $this->logger->Log("Creating record for channel $channel_id.");
            $success = $this->EE->db
                -> insert('exp_purge_settings', $settings);
        }

        // Error check
        if (! $success)
            throw new Exception("Unable to write settings.");

        $this->logger->Log("Settings written to database.");
    }

    

    # ###################
    # Getters and Setters
    # ###################
    public function GetSites ()
    {
        return $this->QuerySites();
    }

    public function GetChannels ($site_id)
    {
        return $this->QueryChannels($site_id);
    }

    public function GetSettings ($channel_id)
    {
        try
        {
            // Load and return settings
            $settings = $this->Load($channel_id);
            $this->logger->Log("Loaded settings for channel $channel_id.");
            $this->logger->Log($settings);
            return $settings;
        }
        catch (Exception $e)
        {
            $this->logger->Log($e->getMessage());

            // Send empty settings object
            return array(
                'purge' => FALSE,
                'uris' => array()
            );
        }
    }

    public function SetSettings ($channel_id, $purge, $uris)
    {
        // Remove empty lines
        foreach ($uris as $key => $uri)
        {
            if (empty($uri))
                unset($uris[$key]);
        }

        // Write settings
        $this->Write($channel_id, $purge, $uris);
    }
}

/* End of file PurgeSettings.php */
/* Location: /system/expressionengine/third_party/purge/PurgeSettings.php */
