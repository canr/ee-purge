<?php
 
/**
 * Creates an accessory to manually enter URLs to be purged from the cache.
 */
class Purge_acc
{	
	public $name			= 'Purge'; //!< Accessory name.
	public $id              = 'purge'; //!< Accessory id string.
	public $version			= '2.2.0'; //!< Accessory version number.
	public $description		= 'Provides a place to manually send purge requests to Varnish.'; //!< Description of the accessory's functionality.
	public $sections		= array();


    private $EE; //!< Reference to the main EE object.

    /**
     * Constructor
     */
    function __construct ()
    {
        $this->EE =& get_instance();
    }
	
	/**
	 * Set Sections
	 */
	public function set_sections()
	{
        // Get ACTion URLs
		$data['ProcessRequestUrl'] = 'index.php?ACT='.$this->EE->cp->fetch_action_id('Purge', 'ProcessPurgeRequest');
		
        // Load view
		$this->sections['Purge URLs From Cache'] = $this->EE->load->view('accessory_purge_varnish', $data, TRUE);
	}
	
}
 
/* End of file acc.purge.php */
/* Location: /system/expressionengine/third_party/purge/acc.purge.php */
