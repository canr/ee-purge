<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed.');
/**
 * Defines a class to manage this module's control panel pages.
 * Each non-constructor function corresponds to a control panel page.
 *
 * @file mcp.purge.php
 * @package Purge
 * @author Eric Slenk <slenkeri@anr.msu.edu>
 */

/** Manages this module's control panel. */
class Purge_mcp
{
	# ###########
 	# Constructor
 	# ###########
    /**
 	 * Constructor.
 	 *
 	 * Hooks EE instance.
 	 */
 	public function __construct()
 	{
 		$this->EE =& get_instance();
 	}
 	
 	
 	# #####
 	# Pages
 	# #####
 	/**
 	 * Control panel's index page.
 	 */
 	public function index()
 	{
 		// Set the title
 		$this->EE->cp->set_variable('cp_page_title', $this->EE->lang->line('purge_module_name'));
 	}
}

