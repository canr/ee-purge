<?php

if ( ! function_exists('send_purge_request'))
{
	/**
	 * Sends purge request to Varnish through CURL
     *
     * Sends a custom request packet with the 'PURGE' header to the given URL.
     * Throws an exception on CURL error.
     *
     * @param string $site_url URL to which the request packet is sent.
     * @param int $site_port Port to which the request packet is sent.
     * @return int Returns the HTTP status code of the cURL request.
	 */
	function send_purge_request($site_url, $site_port=null)
	{
        // Set URL
        if ( ! $site_url)
        {
            // Generate URL for the entire site
            $protocol = (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") ?
                "https://" :
                "http://";
            $purge_url = $protocol . $_SERVER['HTTP_HOST'] . '/';
            $port = isset($_SERVER['SERVER_PORT']) ?
                $_SERVER['SERVER_PORT'] :
                $site_port;
        }
        else
        {
            // Parse out URL for specified part of the site
            $parsed_url = parse_url($site_url);

            // Reconstruct URL using parsed segments
            $url_path = array_key_exists("path", $parsed_url) ?
                $parsed_url["path"] :
                '/';
            $purge_url = $parsed_url["scheme"] . "://" . $parsed_url["host"] . $url_path;
            if ( array_key_exists("query", $parsed_url) )
                $purge_url .= "?".$parsed_url["query"];
            $port = (!array_key_exists("port", $parsed_url) OR
                    is_null($parsed_url["port"])) ?
                80 :
                $parsed_url["port"];
        }

        if (is_null($port))
            $port = 80;

        // Create CURL request
        $curlHandler = curl_init();
        $curlOptList = array(
            CURLOPT_RETURNTRANSFER    => true,
            CURLOPT_CUSTOMREQUEST     => 'PURGE',
            CURLOPT_HEADER            => true,
            CURLOPT_NOBODY            => true,
            CURLOPT_VERBOSE 	      => true,
            CURLOPT_URL               => $purge_url,
            CURLOPT_CONNECTTIMEOUT_MS => 2000,
            CURLOPT_PORT              => $port
        );
        curl_setopt_array( $curlHandler, $curlOptList );

        // Send and error handling
        $response = curl_exec($curlHandler);
        if ( ! $response )
        {
            // Get error data
            $errno = curl_errno($curlHandler);
            $error = curl_error($curlHandler);

            // Close handler
            curl_close($curlHandler);

            // Throw exception
            throw new Exception("CURL Error #$errno: $error");
        }
        
        $status = curl_getinfo($curlHandler, CURLINFO_HTTP_CODE);
        curl_close($curlHandler);
        return (int)$status;
	}
}

/* End of file varnish_helper.php */ 
/* Location: ./system/expressionengine/third_party/purge/helpers/varnish_helper.php */
