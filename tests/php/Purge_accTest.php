<?php
require_once(realpath(dirname(__FILE__).'/../../acc.purge.php'));


class Purge_accTest extends PHPUnit_Framework_TestCase
{
    public function testSet_sections ()
    {
        // Setup
        $purgeAcc = new Purge_acc();

        // Execute
        $purgeAcc->set_sections();

        // Assert
        $this->assertObjectHasAttribute('sections', $purgeAcc);
        $this->assertNotEmpty($purgeAcc->sections);
        $this->assertArrayHasKey('Purge URLs From Cache', $purgeAcc->sections);
        $this->assertEquals('index.php?ACT=127', $purgeAcc->sections['Purge URLs From Cache']);
    }
};


// Mock EE object
function get_instance ()
{
    return new EEMock();
}

class EEMock
{
    public function __construct()
    {
        $this->cp = new CP();
        $this->load = new Load();
    }
};

class CP
{
    public function fetch_action_id ($class, $method)
    {
        return '127';
    }
};

class Load
{
    public function view ($filename, $data, $returnAsData)
    {
        return $data["ProcessRequestUrl"];
    }
};
?>