<?php

// EE Mock
function get_instance()
{
    return new EEMock();
}

class EEMock
{
    public $cp;
    public $db;
    public $dbforge;
    public $load;
    public $logger;

    public function __construct ()
    {
        $this->cp = new CP();
        $this->db = new DB();
        $this->dbforge = new DBForge();
        $this->load = new Load();
        $this->logger = new Logger();
    }
};

class CP
{
    public function fetch_action_id () { }
};

class DB
{
    public $inserts = array();
    public $wheres = array();
    public $deletes = array();

    public function insert ($table, $data)
    {
        $this->inserts[] = array($table, $data);
    }

    public function where ($column, $value)
    {
        $this->wheres[] = array($column, $value);
    }

    public function delete ($table)
    {
        $this->deletes[] = $table;
    }

};

class DBForge
{
    public $fields = array();
    public $keys = array();
    public $tables = array();

    public function add_field ($fields)
    {
        $this->fields = $fields;
    }

    public function add_key ($key, $somethingElse)
    {
        $this->keys[] = $key;
    }

    public function create_table ($tableName)
    {
        $this->tables[$tableName] = array();
    }

    public function drop_table ($table)
    {
        unset($this->tables[$table]);
    }
};

class Load
{
    public function dbforge() { }
    public function helper ($helperName)
    {
        include_once(realpath(dirname(__FILE__).'/../../../helpers/'.$helperName.'_helper.php'));
    }
};

class Logger
{
    public function log () { }
};
?>