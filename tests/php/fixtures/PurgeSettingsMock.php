<?php
class PurgeSettingsMock
{
    
    public function Load ($channel_id)
    {

    }

    public function Write ($channel_id, $purge, $uris)
    {

    }

    public function GetSites ()
    {

    }

    public function GetChannels ($site_id)
    {

    }

    public function GetSettings ($channel_id)
    {
        switch ($channel_id)
        {
            case 1:
                return array(
                    'purge' => true,
                    'uris' => array(
                        'http://expengdev.anr.msu.edu/num1',
                        'http://expengdev.anr.msu.edu/num2'
                    )
                );
            case 2:
                return array(
                    'purge' => true,
                    'uris' => array()
                );
            case 3:
                return array(
                    'purge' => false,
                    'uris' => array()
                );
            default:
                throw new Exception("Channel not found.");
        }

        // else if ($channel_id === 2)
        // {
        //     return array (
        //         'purge' => true,
        //         'uris' => array(
        //         )
        //     );
        // }
        // else if ($channel_id === 3)
        // {
        //     return array(
        //         'purge' => false,
        //         'uris' => array()
        //     );
        // }
        // else if ($channel_id === 4)
        // {
        //     return array(
        //         'purge' => true,
        //         'uris' => array(
        //             '{base_url}/index.html',
        //             'http://expengdev.anr.msu.edu/entry{entry_id}.html',
        //             'http://expengdev.anr.msu.edu/{site_id}/index.html',
        //             'http://expengdev.anr.msu.edu/{channel_id}/index.html',
        //             'http://expengdev.anr.msu.edu/{url_title}',
        //             'http://expengdev.anr.msu.edu/{structure_uri}'
        //         )
        //     );
        // }
    }

    public function SetSettings ($channel_id)
    {

    }
};
?>