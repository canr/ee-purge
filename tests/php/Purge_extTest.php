<?php
require_once(realpath(dirname(__FILE__).'/../../ext.purge.php'));
require_once(realpath(dirname(__FILE__).'/fixtures/EEMock.php'));
require_once(realpath(dirname(__FILE__).'/fixtures/PurgeSettingsMock.php'));

class Purge_extTest extends PHPUnit_Framework_Testcase
{
    public function testAttributes ()
    {
        $purgeExt = new Purge_ext();

        $this->assertObjectHasAttribute('description', $purgeExt);
        $this->assertObjectHasAttribute('docs_url', $purgeExt);
        $this->assertObjectHasAttribute('name', $purgeExt);
        $this->assertObjectHasAttribute('settings_exist', $purgeExt);
        $this->assertObjectHasAttribute('version', $purgeExt);
    }

    public function testHooks ()
    {
        $purgeExt = new Purge_ext();

        // All hooks are in the array
        $this->assertObjectHasAttribute('hooks', $purgeExt);
        $this->assertNotEmpty($purgeExt->hooks);
        $this->assertArrayHasKey('entry_submission_end', $purgeExt->hooks);
        $this->assertArrayHasKey('delete_entries_loop', $purgeExt->hooks);
    }

    public function testActivate_extension ()
    {
        $purgeExt = new Purge_ext();
        $purgeExt->activate_extension();

        // Settings table fields
        $this->assertObjectHasAttribute('fields', $purgeExt->EE->dbforge);
        $this->assertNotEmpty($purgeExt->EE->dbforge->fields);
        $this->assertArrayHasKey('channel_id', $purgeExt->EE->dbforge->fields);
        $this->assertArrayHasKey('purge', $purgeExt->EE->dbforge->fields);
        $this->assertArrayHasKey('uris', $purgeExt->EE->dbforge->fields);
        $this->assertEquals($purgeExt->EE->dbforge->fields['channel_id'], array('type' => 'int', 'unsigned' => TRUE));
        $this->assertEquals($purgeExt->EE->dbforge->fields['purge'], array('type' => 'boolean'));
        $this->assertEquals($purgeExt->EE->dbforge->fields['uris'], array('type' => 'text'));

        // Set up table key
        $this->assertNotEmpty($purgeExt->EE->dbforge->keys);
        $this->assertContains('channel_id', $purgeExt->EE->dbforge->keys);

        // Hooks are in the database
        $this->assertNotEmpty($purgeExt->EE->db->inserts);
        $this->assertContains(array('extensions', array(
            'class'     => 'Purge_ext',
            'method'    => 'purge_submitted_entry',
            'hook'      => 'entry_submission_end',
            'settings'  => '',
            'version'   => $purgeExt->version,
            'enabled'   => 'y'
        )), $purgeExt->EE->db->inserts);
        $this->assertContains(array('extensions', array(
            'class'     => 'Purge_ext',
            'method'    => 'purge_deleted_entry',
            'hook'      => 'delete_entries_loop',
            'settings'  => '',
            'version'   => $purgeExt->version,
            'enabled'   => 'y'
        )), $purgeExt->EE->db->inserts);
    }

    public function testDisable_extension ()
    {
        $purgeExt = new Purge_ext();
        $purgeExt->activate_extension();
        $purgeExt->disable_extension();

        // Settings table dropped
        $this->assertArrayNotHasKey('purge_settings', $purgeExt->EE->dbforge->tables);

        // Extensions removed from extensions table
        $this->assertObjectHasAttribute('wheres', $purgeExt->EE->db);
        $this->assertContains(array('class', 'Purge_ext'), $purgeExt->EE->db->wheres);
        $this->assertObjectHasAttribute('deletes', $purgeExt->EE->db);
        $this->assertContains('extensions', $purgeExt->EE->db->deletes);
    }

    public function testUpdate_extension ()
    {
        $purgeExt = new Purge_ext();
        $this->assertFalse($purgeExt->update_extension());
    }

    public function testShouldBePurged ()
    {
        $purgeExt = new Purge_ext();
        $purgeExt->settings = new PurgeSettingsMock();

        // Should not
        $this->assertFalse($purgeExt->ShouldBePurged(3));

        // Should
        $this->assertTrue($purgeExt->ShouldBePurged(1));
    }

    public function testGetPurgeUris ()
    {
        $purgeExt = new Purge_ext();
        $purgeExt->settings = new PurgeSettingsMock();

        // Empty
        print_r($purgeExt->settings->GetSettings(2));
        // $this->assertEmpty($purgeExt->GetPurgeUris(2, 2));

        // Results
        // $this->assertNotEmpty($purgeExt->GetPurgeUris(1, 1));
        // $this->assertEquals($purgeExt->GetPurgeUris(1, 1), array(
        //     'http://expengdev.anr.msu.edu/num1',
        //     'http://expengdev.anr.msu.edu/num2'
        // ));


        // Results with tags

        // Channel not found
        
    }

    public function testSendPurgeRequest ()
    {
    }
    
};
?>